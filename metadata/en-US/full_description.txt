Quickly show all possible android states for all installed packages:

    SharedLibs & uid added

    Persistent +Stopped +Inactive +Suspended state per app

    debug or test possible flavour

    sdk version (sorting option for Oreo/Pie)

    READ_LOGS if granted

    system app with locked data

    ALL ApplicationInfos classic features

    (full scan classes & trackers via intent/ClassyShark3xodus)

With minimum of permissions, this app also demonstrates all possible leaks that can be done by any installed app with INTERNET granted (and closed sources), in order to grab data for profiling.

(prim-origin https://github.com/MajeurAndroid/Android-Applications-Info)